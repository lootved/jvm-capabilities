package main

import (
	"bufio"
	"flag"
	"log"
	"os"
)

func main() {
	input := "data/input.txt"
	output := "data/res.kt"
	flag.StringVar(&input, "i", input, "input file to process")
	flag.StringVar(&output, "o", output, "output path")
	flag.Parse()

	f, err := os.Open(input)
	check(err)
	defer f.Close()
	sc := bufio.NewScanner(f)

	sc.Split(bufio.ScanLines)
	lines := []string{}

	for sc.Scan() {
		bline := sc.Bytes()
		v := processLine(bline)
		if len(v) > 0 {
			//res = append(res, v...)
			lines = append(lines, string(v))
		}
	}
	generateCode(lines, output)
	// os.WriteFile(output, res, 0644)
}

func generateCode(lines []string, output string) {
	res := "import kotlinx.serialization.*\n"
	res += "import kotlinx.serialization.json.*\n\n\n"

	res += "val map = HashMap<String, Boolean>()\n"
	for _, line := range lines {
		res += `map.put("` + line + `", vm.` + line + "())\n"
	}
	res += " val props = Json.encodeToString(map) \n"
	os.WriteFile(output, []byte(res), 0644)
}

func processLine(bline []byte) []byte {
	size := len(bline)
	idx := 0
	//find first space
	for idx < size && !isSpace(bline[idx]) {
		idx++
	}
	// skip over all spaces
	for idx < size && isSpace(bline[idx]) {
		idx++
	}
	if idx == size {
		return []byte{}
	}
	start := idx
	for idx < size && bline[idx] != '(' {
		idx++
	}
	if bline[idx] == '(' {
		return bline[start:idx]
	}
	return []byte{}
}

func isSpace(c byte) bool {
	return c == ' ' || c == '\t'
}

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
